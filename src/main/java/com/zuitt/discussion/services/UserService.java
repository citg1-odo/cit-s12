package com.zuitt.discussion.services;

import com.zuitt.discussion.models.User;
import org.springframework.http.ResponseEntity;

public interface UserService {

    void createUser(User user);

    Iterable<User> getUsers();

    ResponseEntity updateUser(Long id, User user);

    ResponseEntity deleteUser(Long id);
}
