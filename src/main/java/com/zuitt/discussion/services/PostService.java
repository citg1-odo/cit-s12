// Service is an interface that exposes the methods of an implementation whose details have been abstracted away.
package com.zuitt.discussion.services;

import com.zuitt.discussion.models.Post;
import org.springframework.http.ResponseEntity;

public interface PostService {

    void createPost(Post post);

    Iterable<Post> getPosts();

    ResponseEntity deletePost(Long id);

    ResponseEntity updatePost(Long id, Post post);
}
